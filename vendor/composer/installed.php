<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => '17b4bef21b58995e5cd4e77778624ea2777f253d',
        'name' => 'setone/tell-me',
        'dev' => true,
    ),
    'versions' => array(
        'setone/tell-me' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => '17b4bef21b58995e5cd4e77778624ea2777f253d',
            'dev_requirement' => false,
        ),
    ),
);
