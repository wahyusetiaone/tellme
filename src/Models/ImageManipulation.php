<?php

namespace Setone\TellMe\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageManipulation extends Model
{
    use HasFactory;
    /*
    |--------------------------------------------------------------------------
    | Your Relation Model
    |--------------------------------------------------------------------------
    |
    | Discription for fillable bellow :
    | image_id => is a id for pointing to table images
    | slug => mean is a name of manipulate image like 'icon_profile', 'thumbnail', etc.
    | name => this is name of image like 'small', 'md', 'large_profile', etc
    | size => this is json with format {width : 20, heigth : 20}
    | path => mean location to save this image
    */

    protected $fillable = [
        'image_id',
        'slug',
        'name',
        'size',
        'path',
    ];

    /*
    |--------------------------------------------------------------------------
    | Your Relation Model
    |--------------------------------------------------------------------------
    |
    | Here is where you can register your relation models.
    |
    */
}
