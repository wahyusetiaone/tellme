<?php

namespace Setone\TellMe\Providers;
use Illuminate\Support\ServiceProvider;
use Setone\TellMe\Helpers\TellMe;

class TellMeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('tellme',function(){
            return new TellMe();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }
}
